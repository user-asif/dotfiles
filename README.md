# i3 dotfiles

![Semantic description of image](/images/screen-1.png)

![Semantic description of image](/images/screen-3.png)

![Semantic description of image](/images/screen-2.png)

![Semantic description of image](/images/screen-4.png)

Distro: Arch Linux

Terminal: Alacritty, cool-retro-term

Shell: Fish

Editor: vim

Launcher: Rofi

Files: ranger

Bar: i3bar, i3status

Others: jp2a, neofetch, htop, cmatrix

<div class="panel panel-warning">
**Warning**
<div class="panel-body">

please check for changes in the config related to paths of img or dir

</div>
</div>

install the following fonts

[Operator mono nerd font](https://github.com/40huo/Patched-Fonts/tree/master/operator-mono-nerd-font)

[Share Tech mono nerd font](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/ShareTechMono)

[Nikkyou sans serif](https://fonts2u.com/download/nikkyou-sans.font)

[Iosevka nerd font](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Iosevka)
